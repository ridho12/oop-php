<?php
    require('animal.php');
    require('ape.php');
    require('frog.php');

    $sheep = new Animal("shaun");

    echo "name : " . $sheep->name . "<br>"; // "shaun"
    echo "legs : " . $sheep->legs. "<br>"; // 2
    echo "Cold Blooded : ". $sheep->cold_blooded . "<br>";// false
    
    $sungokong = new Ape("kera sakti");
    $sungokong->yell();// "Auooo"
    echo "<br>";

    $kodok = new Frog("buduk");
    $kodok->jump() ; // "hop hop"
?>